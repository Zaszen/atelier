<?php
class VueAdherent{
	private $DivResultat, $listDocs, $listCats, $listGenres, $document;

	public function __get($attr_name) {
	    if (property_exists( __CLASS__, $attr_name)) { 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
	    throw new Exception($emess, 45);
	  }
	   
	    public function __set($attr_name, $attr_val) {
	    if (property_exists( __CLASS__, $attr_name)) {
	      $this->$attr_name=$attr_val; 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
	    throw new Exception($emess, 45);
	    
	  }

	private function afficheRecherche(){
		$res=' <div class="row">
				<div class="offset-1 span-10" >
				<h2 class="text-center"> Rechercher un document</h2>
			<form method="post" action="index.php?action=Rechercher">
				<fieldset>
				<input type="text" name="titre" id="titre" placeholder="Titre"/>
				</fieldset>
				 <fieldset>
				 	<legend>Affiner la recherche</legend>
				 	<input type="text" name="artiste" id="artiste" placeholder="Artiste" />
				 	<select name="categorie" placeholder="categorie"  id="categorie">
				 	<option value="" name="" selected>Sélectionnez un type</option>';
		foreach ($this->listCats as $val) {
			$res.='<option value="'.$val->IdCategorie.'" name="'.$val->IdCategorie.'">'.$val->NomC.'</option>';
		}
		$res.='</select>
				<select name="genre" placeholder="genre" id="genre">
				<option value="" name="" selected>Sélectionnez un genre</option>';
		foreach ($this->listGenres as $val) {
			$res.='<option value="'.$val->IdGenre.'" name="'.$val->IdGenre.'" id="'.$val->IdGenre.'">'.$val->NomG.'</option>';
		}
		$res.='</select>
		<input type="submit" class="btn btn-green right" value="Rechercher" />
		</fieldset></form></div></div>';

		if(isset($_POST['titre'])){
			$listeDocs=Document::findByDonnees($_POST['titre'],$_POST['artiste'],$_POST['categorie'],$_POST['genre']);
			if(is_array($listeDocs)){
				if(isset($listeDocs[0])){
					$res.='<div class="row">
					<div class="offset-1 span-10">
					<div class="resultat" >';
					foreach($listeDocs as $doc){
						$res.='<article>
						<img src="'.$doc->UrlImage.'" alt="image" />
						<p>'.$doc->Titre.' par '.$doc->Artiste.' <hr/><br/>
						<a class="text-center" href="index.php?action=Detail&id='.$doc->RefDocument.'"> voir le détail</a>
						</p>

						<div class='.$doc->Disponibilite.' ><span>'.$doc->Disponibilite.'</span></div>
							</article>';
					}
					$res.='</div>';
				}
				else{$res.='<div class="error"><span>Aucun résultat</span></div>';}
			}
			else{$res.='<div class="error"><span>Aucun résultat</span></div>';}
			$res.='</div></div>';
		}

		return $res;
	}

	private function AfficheHeader(){
    $var="<img src=\"images/banniere.jpg\" />";
    return $var;
	}


 private function AfficheFooter(){
	 	$var="<footer>
        <p>Â&copy; 2014  | Mediatheque Nancy</p>
    	</footer>";
    	return $var;
	 }

	private function AfficheDetail(){
		$categorie=Categorie::findById($this->document->IdCategorie);
		$genre=Genre::findById($this->document->IdGenre);
		$res='<div class="row">
		<div class="offset-1 span-3">';
		$res.='<img src="'.$this->document->UrlImage.'" alt="image" /></div>
		<div class="span-2"><h2>Titre : '.$this->document->Titre.'<h2></div>';
		$res.='<div class="span-5"><p>Artiste : '.$this->document->Artiste.' - Type : '.$categorie->NomC.' - Genre : '.$genre->NomG.'</p>';
		$res.='<p>Descriptif : '.$this->document->Descriptif.'</p>';
		$res.='<a class="btn btn-green" href="index.php?action=Rechercher">retour</a></div></div></div>';
		return $res;
	}


		public function afficheGeneral($sel){
			$html = "<!DOCTYPE html><html lang='fr'><head>
			<meta charset='UTF-8'><title>Médiathèque de Nancy</title>
			<link rel='stylesheet' type='text/css' href='stylesheets/style.css'>
			</head><body><div class='container' >";
			$html .= "<header>".$this->AfficheHeader()."</header>";
			switch ($sel) {
				case 'Rechercher':
					$html .= "<div class='recherche'>".$this->afficheRecherche()."</div>";
					break;
				case 'Detail':
					$html .= "<div class='recherche'>".$this->AfficheDetail()."</div>";
				break;
			}
			$html .= $this->AfficheFooter();
			$html .= "</div></body></html>";

			echo $html;
		}
}
?>