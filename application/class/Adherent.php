<?php

class Adherent {

  private $IdAdherent, $Nom, $Prenom, $Adress, $email, $Tel; 

  
  public function __construct() {
    
  }

  public function __toString() {
        return "[". __CLASS__ . "] IdAdherent : ". $this->IdAdherent . ":
				   Nom  ". $this->Nom  .":
           Prenom  ". $this->Prenom  .":
           Adress  ". $this->Adress  .":
           email  ". $this->email  .":
				   Tel ". $this->Tel  ;
  }

  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    throw new Exception($emess, 45);
  }
   
    public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) {
      $this->$attr_name=$attr_val; 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    throw new Exception($emess, 45);
    
  }

  public function save() {
    if (!isset($this->IdAdherent)) {
      return $this->insert();
    } else {
      return $this->update();
    }
  }


  public function update() {
    
    if (!isset($this->IdAdherent)) {
      throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
    } 
    
    $save_query = "update Adherents set Nom=".(isset($this->Nom) ? "'$this->Nom'" : "null").",
                                    Prenom=".(isset($this->Prenom) ? "'$this->Prenom'" : "null").",
                                    Adress=".(isset($this->Adress) ? "'$this->Adress'" : "null").",
                                    email=".(isset($this->email) ? "'$this->email'" : "null").",
                                    Tel=".(isset($this->Tel) ? "'$this->Tel'" : "null"). "where IdAdherent=".$this->IdAdherent;
    $pdo = Base::getConnexion();
    $nb=$pdo->exec($save_query);
    
	return $nb;
    
  }

  public function delete() {

    try{
      $query="delete FROM Adherents WHERE IdAdherent=".$this->IdAdherent;
      $pdo = Base::getConnexion();
      $dbres = $pdo->exec($query);
    }catch (PDOException $e) {
      throw new PDOException($e->getMessage());
    }

	  return $dbres;
	 
	 
  }
										
  public function insert() {
	  try{
      $query="INSERT INTO Adherents VALUES (null,'".$this->Nom."','".$this->Prenom."','".$this->Adress."','".$this->email."','".$this->Tel."')";
      $pdo = Base::getConnexion();
      $aff_rows = $pdo->exec($query);
      $this->IdAdherent=$pdo->LastInsertId();

      return $aff_rows;
    }catch (PDOException $e) {
      throw new PDOException($e->getMessage());
    }
	
  }
		
    public static function findById($IdAdherent) {
      $query = "select * from Adherents where IdAdherent=:IdAdherent";
      //echo $query;

      try{
      $pdo = Base::getConnexion();
      $stmt = $pdo->prepare($query);
      $stmt->execute(array(':IdAdherent' => $IdAdherent));
      
      $dbres=$stmt->fetch(PDO::FETCH_OBJ) ;
      if(is_object($dbres)){
        $a = new Adherent();
        $a->IdAdherent= $dbres->IdAdherent;
        $a->Nom= $dbres->Nom;
        $a->Prenom= $dbres->Prenom;
        $a->Adress= $dbres->Adress;
        $a->email= $dbres->email;
        $a->Tel= $dbres->Tel;
      }
      else{$a='';}

      }catch (PDOException $e) {
        throw new PDOException($e->getMessage());
      }
	   return $a;
	   
    }

    public static function findAll() {

  	   try {
        $query = "select * from Adherents" ;
        $pdo = Base::getConnexion();
        $res=$pdo->query($query);
        $tres=$res->fetchall(PDO::FETCH_OBJ);
        $tab=array();
        foreach($tres as $c){
          $o=new Adherent();
          $o->IdAdherent=$c->IdAdherent;
          $o->Nom=$c->Nom;
          $o->Prenom=$c->Prenom;
          $o->email=$c->email;
          $o->Adress=$c->Adress;
          $o->Tel=$c->Tel;
          $tab[]=$o;
        }
      } catch (PDOException $e) {
       echo $query . "<br>";
      throw new PDOException("Erreur requête".$e>getMessage());
      }
      return $tab;
    }
}



?>
