<?php

class Emprunt {

  private $id, $IdAdherent, $RefDocument, $DateEmprunt; 

  
  public function __construct() {
    
  }

  /*public function __toString() {
        return "[". __CLASS__ . "] IdAdherent : ". $this->IdAdherent . ":
				   RefDocument  ". $this->RefDocument  .":
				   DateEmprunt ". $this->DateEmprunt  ;
  }*/

  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    throw new Exception($emess, 45);
  }
   
    public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) {
      $this->$attr_name=$attr_val; 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    throw new Exception($emess, 45);
    
  }

  public function save() {
    if (!isset($this->id)) {
      return $this->insert();
    } else {
      return $this->update();
    }
  }


  public function update() {
    
    if (!isset($this->id)) {
      throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
    } 
    
    $save_query = "update Emprunts set RefDocument=".(isset($this->RefDocument) ? "'$this->RefDocument'" : "null").",
                                    IdAdherent=".(isset($this->IdAdherent) ? "'$this->IdAdherent'" : "null").",
                                    DateEmprunt=".(isset($this->DateEmprunt) ? "'$this->DateEmprunt'" : "null"). "where id=".$this->id;
    $pdo = Base::getConnexion();
    $nb=$pdo->exec($save_query);
    
	return $nb;
  }

  public function delete() {

    try{
      $query="delete FROM Emprunts WHERE id=".$this->id;
      $pdo = Base::getConnexion();
      $dbres = $pdo->exec($query);
    }catch (PDOException $e) {
      throw new PDOException($e->getMessage());
    }
	  return $dbres;
  }
										
  public function insert() {
    $this->DateEmprunt=date("y-m-d h:i:s");
	  try{
      $query='INSERT INTO Emprunts VALUES (null,'.$this->IdAdherent.','.$this->RefDocument.',"'.$this->DateEmprunt.'")';
      $pdo = Base::getConnexion();
      $aff_rows = $pdo->exec($query);
      $this->id=$pdo->LastInsertId();

      return $aff_rows;
    }catch (PDOException $e) {
      throw new PDOException($e->getMessage());
    }
	
  }

  public static function deleteDocument($id)
  {
    try{
      $query="delete FROM Emprunts WHERE RefDocument=".$id;
      $pdo = Base::getConnexion();
      $dbres = $pdo->exec($query);
    }catch (PDOException $e) {
      throw new PDOException($e->getMessage());
    }

    return $dbres;
  }
		
    public static function findByIdAdherent($id) {
      $query = "select * from Emprunts where IdAdherent=:IdAdherent";
      //echo $query;

      try{
      $pdo = Base::getConnexion();
      $stmt = $pdo->prepare($query);
      $stmt->execute(array(':IdAdherent' => $IdAdherent));
      
      $dbres=$stmt->fetch(PDO::FETCH_OBJ) ;
     
        $a = new Emprunt();
        $a->id=$dbres->id;
        $a->IdAdherent= $dbres->IdAdherent;
        $a->RefDocument= $dbres->RefDocument;
        $a->DateEmprunt= $dbres->DateEmprunt;

      }catch (PDOException $e) {
        throw new PDOException($e->getMessage());
      }
	   return $a;
    }

    public static function findAll() {

  	   try {
        $query = "select * from Emprunts " ;
        $pdo = Base::getConnexion();
        $res=$pdo->query($query);
        $tres=$res->fetchall(PDO::FETCH_OBJ);
        $tab=array();
        foreach($tres as $c){
          $o=new Emprunt();
          $o->id=$c->id;
          $o->IdAdherent=$c->IdAdherent;
          $o->RefDocument=$c->RefDocument;
          $o->DateEmprunt=$c->DateEmprunt;
          $tab[]=$o;
        }
      } catch (PDOException $e) {
       echo $query . "<br>";
      throw new PDOException("Erreur requête".$e>getMessage());
      }
      return $tab;
    }

   public static function findByRef($RefDocument) {
      $query = "select * from Emprunts where RefDocument=:RefDocument";
      //echo $query;

      try{
      $pdo = Base::getConnexion();
      $stmt = $pdo->prepare($query);
      $stmt->execute(array(':RefDocument' => $RefDocument));
      
      $dbres=$stmt->fetch(PDO::FETCH_OBJ) ;
      if(is_object($dbres)){
        $a = new Emprunt();
        $a->id=$dbres->id;
        $a->IdAdherent=$dbres->IdAdherent;
        $a->RefDocument=$dbres->RefDocument;
        $a->DateEmprunt=$dbres->DateEmprunt;
      }
      else{
        $a='';
      }
      }catch (PDOException $e) {
        throw new PDOException($e->getMessage());
      }
     return $a;
     
    }
}



?>
