<?php
class ControllerAdherent{


	private function actionRecherche(){
		$vue=new VueAdherent();
		$listCats=Categorie::findAll();
		$listGenres=Genre::findAll();

		$vue->listCats=$listCats;
		$vue->listGenres=$listGenres;
		$vue->afficheGeneral("Rechercher");
	}

	private function actionDetail()
	{
		$vue=new VueAdherent();
		$d=Document::findByRef($_GET['id']);
		$listCats=Categorie::findAll();
		$listGenres=Genre::findAll();

		$vue->document=$d;
		$vue->listCats=$listCats;
		$vue->listGenres=$listGenres;
		$vue->afficheGeneral("Detail");

	}

	public function dispatch($get){
		if(isset($get['action'])){
			switch($get['action']){
				case 'Detail':
					$this->actionDetail();
				break;
				case 'Rechercher':
					$this->actionRecherche();
				break;
				default:
					$this->actionRecherche();
				break;
			}
		}
		else {$v=new VueAdherent();
		  $this->actionRecherche();}
	 }
}
?>
