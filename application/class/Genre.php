<?php

class Genre {

  private $IdGenre, $NomG; 

  
  public function __construct() {
    
  }

  public function __toString() {
        return "[". __CLASS__ . "] IdGenre : ". $this->IdGenre . ":
				   nomC  ". $this->nomC  ;
  }

  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    throw new Exception($emess, 45);
  }
   
    public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) {
      $this->$attr_name=$attr_val; 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    throw new Exception($emess, 45);
    
  }

  public function save() {
    if (!isset($this->IdGenre)) {
      return $this->insert();
    } else {
      return $this->update();
    }
  }


  public function update() {
    
    if (!isset($this->IdGenre)) {
      throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
    } 
    
    $save_query = "update Genres set NomG=".(isset($this->NomG) ? "'$this->NomG'" : "null"). "where IdGenre=".$this->IdGenre;
    $pdo = Base::getConnexion();
    $nb=$pdo->exec($save_query);
    
	return $nb;
    
  }

  public function delete() {

    try{
      $query="delete FROM Genres WHERE IdGenre=".$this->IdGenre;
      $pdo = Base::getConnexion();
      $dbres = $pdo->exec($query);
    }catch (PDOException $e) {
      throw new PDOException($e->getMessage());
    }

	  return $dbres;
	 
	 
  }
										
  public function insert() {
	  try{
      $query="INSERT INTO Genres VALUES (null,'".$this->NomG."')";
      $pdo = Base::getConnexion();
      $aff_rows = $pdo->exec($query);
      $this->IdGenre=$pdo->LastInsertId();

      return $aff_rows;
    }catch (PDOException $e) {
      throw new PDOException($e->getMessage());
    }
	
  }
		
    public static function findById($IdGenre) {
      $query = "select * from Genres where IdGenre=:IdGenre";
      //echo $query;

      try{
      $pdo = Base::getConnexion();
      $stmt = $pdo->prepare($query);
      $stmt->execute(array(':IdGenre' => $IdGenre));
      
      $dbres=$stmt->fetch(PDO::FETCH_OBJ) ;
     
        $a = new Genre();
        $a->IdGenre= $dbres->IdGenre;
        $a->NomG= $dbres->NomG;

      }catch (PDOException $e) {
        throw new PDOException($e->getMessage());
      }
	   return $a;
	   
    }

    public static function findAll() {

  	   try {
        $query = "select * from Genres" ;
        $pdo = Base::getConnexion();
        $res=$pdo->query($query);
        $tres=$res->fetchall(PDO::FETCH_OBJ);
        $tab=array();
        foreach($tres as $c){
          $o=new Genre();
          $o->IdGenre=$c->IdGenre;
          $o->NomG=$c->NomG;
          $tab[]=$o;
        }
      } catch (PDOException $e) {
       echo $query . "<br>";
      throw new PDOException("Erreur requête".$e>getMessage());
      }
      return $tab;
    }
}



?>
