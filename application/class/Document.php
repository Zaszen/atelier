<?php
	class Document
	{
		private $RefDocument, $Artiste, $Titre, $Descriptif, $IdGenre, $IdCategorie, $UrlImage, $Disponibilite;

		public function __construct()
		{}

		public function __get($attr_name) 
		{
	    	if (property_exists( __CLASS__, $attr_name)) 
	    	{ 
	      		return $this->$attr_name;
	    	} 
	    	$emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
	    	throw new Exception($emess, 45);
	  	}
	   
	    public function __set($attr_name, $attr_val) 
	    {
	   		if (property_exists( __CLASS__, $attr_name)) 
	   		{
	      		$this->$attr_name=$attr_val; 
	      		return $this->$attr_name;
	    	} 
	    	$emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
	    	throw new Exception($emess, 45);
		}

		public function save() 
		{
		    if (!isset($this->RefDocument)) 
		    {
		      return $this->insert();
		    } 
		    else 
		    {
		      return $this->update();
    		}
    	}

    	 public function updateDispo() {
    
		    if (!isset($this->RefDocument)) {
		      throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
		    } 
		    $save_query = "update Documents set Disponibilite=".(isset($this->Disponibilite) ? "'$this->Disponibilite'" : "null"). "where RefDocument=".$this->RefDocument;
		    $pdo = Base::getConnexion();
		    $nb=$pdo->exec($save_query);
		    
			return $nb;
		    
		  }

    	public function delete() 
    	{
			try{
      		$query="delete FROM Documents WHERE RefDocument=".$this->RefDocument;
      		$pdo = Base::getConnexion();
      		$dbres = $pdo->exec($query);
    		}catch (PDOException $e) {
      			throw new PDOException($e->getMessage());
    		}

	  		return $dbres;
	 	}

	 	public function insert()
	 	{
	 		try{
      		$query="INSERT INTO Documents VALUES (null,'".$this->Titre."','".$this->Descriptif."','".$this->IdGenre."','".$this->IdCategorie."','".$this->UrlImage."','".$this->Disponibilite."')";

     		$pdo = Base::getConnexion();
      		$aff_rows = $pdo->exec($query);
     		$this->RefDocument=$pdo->LastInsertId();
     		return $aff_rows;
    		}catch (PDOException $e) {
      			throw new PDOException($e->getMessage());
    		}
		}

	public static function findByRef($RefDocument) {
      $query = "select * from Documents where RefDocument=:RefDocument";
      //echo $query;

      try{
      $pdo = Base::getConnexion();
      $stmt = $pdo->prepare($query);
      $stmt->execute(array(':RefDocument' => $RefDocument));
      
      $dbres=$stmt->fetch(PDO::FETCH_OBJ) ;
      if(is_object($dbres)){
        $a = new Document();
        $a->RefDocument=$dbres->RefDocument;
        $a->Titre=$dbres->Titre;
        $a->Artiste=$dbres->Artiste;
      	$a->Descriptif=$dbres->Descriptif;
      	$a->IdGenre=$dbres->IdGenre;
      	$a->IdCategorie=$dbres->IdCategorie;
      	$a->UrlImage=$dbres->UrlImage;
      	$a->Disponibilite=$dbres->Disponibilite;
      }
      else{$a='';}
      }catch (PDOException $e) {
        throw new PDOException($e->getMessage());
      }
	   return $a;
	   
    }

	public static function findByDonnees($titre='', $artiste='', $idcategorie='', $idgenre='') {

       try {
        $query = "select * from Documents where ";
        if($titre!=''){$query.="Titre like '%".$titre."%'" ;}
        if($artiste!='' and $titre==''){$query.="Artiste like '%".$artiste."%' " ;}
        elseif(!$artiste==''){$query.="and Artiste like '%".$artiste."%' " ;}
        if($idcategorie!='' and $titre=='' and $artiste==''){$query.="IdCategorie=".$idcategorie;}
        elseif(!$idcategorie==''){$query.="and IdCategorie=".$idcategorie;}
        if($idgenre!='' and $titre=='' and $artiste=='' and $idcategorie==''){$query.="IdGenre=".$idgenre;}
        elseif(!$idgenre==''){$query.="and IdGenre=".$idgenre;}
        $pdo = Base::getConnexion();
        $res=$pdo->query($query);
        if(is_object($res)){
	        $tres=$res->fetchall(PDO::FETCH_OBJ);
	        $tab=array();
	        foreach($tres as $c){
	          	$o=new Document();
	         	$o->RefDocument=$c->RefDocument;
	          	$o->Titre=$c->Titre;
	          	$o->Artiste=$c->Artiste;
	          	$o->Descriptif=$c->Descriptif;
	          	$o->IdGenre=$c->IdGenre;
	          	$o->IdCategorie=$c->IdCategorie;
	          	$o->UrlImage=$c->UrlImage;
	          	$o->Disponibilite=$c->Disponibilite;
	          	$tab[]=$o;
	        }
		}
		else{$tab='';}
      } catch (PDOException $e) {
       echo $query . "<br>";
      throw new PDOException("Erreur requête".$e>getMessage());
      }
      return $tab;
    }

    	public static function findAll() 
	   	{
			try{
        		$query = "select * from Documents " ;
        		$pdo = Base::getConnexion();
		        $res=$pdo->query($query);
		        $tres=$res->fetchall(PDO::FETCH_OBJ);
		        $tab=array();
		        foreach($tres as $c){
			        $o=new Document();
			        $o->RefDocument=$c->RefDocument;
			        $o->Titre=$c->Titre;
			        $o->Artiste=$c->Artiste;
			        $o->Descriptif=$c->Descriptif;
			        $o->IdGenre=$c->IdGenre;
			        $o->IdCategorie=$c->IdCategorie;
			        $o->UrlImage=$c->IdCategorie;
			        $o->Disponibilite=$c->Disponibilite;
			        $tab[]=$o;
	        	}
      		} catch (PDOException $e) {
        		echo $query . "<br>";
      			throw new PDOException("Erreur requête".$e>getMessage());
      		}
      		return $tab;
    	}

    

    	public static function findTop5() 
	   	{
			try{
        		$query = "select * from Documents limit 5 " ;
        		$pdo = Base::getConnexion();
		        $res=$pdo->query($query);
		        $tres=$res->fetchall(PDO::FETCH_OBJ);
		        $tab=array();
		        foreach($tres as $c){
			        $o=new Document();
			        $o->RefDocument=$c->RefDocument;
			        $o->Titre=$c->Titre;
			        $o->Artiste=$c->Artiste;
			        $o->Descriptif=$c->Descriptif;
			        $o->IdGenre=$c->IdGenre;
			        $o->IdCategorie=$c->IdCategorie;
			        $o->UrlImage=$c->IdCategorie;
			        $o->Disponibilite=$c->Disponibilite;
			        $tab[]=$o;
	        	}
      		} catch (PDOException $e) {
        		echo $query . "<br>";
      			throw new PDOException("Erreur requête".$e>getMessage());
      		}
      		return $tab;
    	}

	}
?>