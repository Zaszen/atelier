<?php
class VueStaff{
	private $header, $center, $listCats, $listGenres, $footer;

	public function __get($attr_name) {
	    if (property_exists( __CLASS__, $attr_name)) { 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
	    throw new Exception($emess, 45);
	  }
	   
	    public function __set($attr_name, $attr_val) {
	    if (property_exists( __CLASS__, $attr_name)) {
	      $this->$attr_name=$attr_val; 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
	    throw new Exception($emess, 45);
	    
	  }

	private function afficheRecherche(){
		$res='<div class="row">
				<div class="offset-1 span-10">
				<h2 class="text-center">Recherche</h2>
			<form method="post" action="admin.php?action=Rechercher">
				<fieldset>
					<label for="titre">Rechercher un document</label>
					<input type="text" name="titre" id="titre" placeholder="Titre"/>
				</fieldset>

				 <fieldset>
				 	<legend>Affiner la recherche</legend>
				 	<input type="text" name="artiste" id="artiste" placeholder="Artiste" />
				 	<select name="categorie" placeholder="categorie"  id="categorie">
				 	<option value="" name="" selected>Sélectionnez un type</option>';
		foreach ($this->listCats as $val) {
			$res.='<option value="'.$val->IdCategorie.'" name="'.$val->IdCategorie.'">'.$val->NomC.'</option>';
		}
		$res.='</select>
				<select name="genre" placeholder="genre" id="genre">
				<option value="" name="" selected>Sélectionnez un genre</option>';
		foreach ($this->listGenres as $val) {
			$res.='<option value="'.$val->IdGenre.'" name="'.$val->IdGenre.'" id="'.$val->IdGenre.'">'.$val->NomG.'</option>';
		}
		$res.='</select>
		<input type="submit" class="btn btn-red right" value="Rechercher" />
		</fieldset></form></div></div>';
			$res.='<div class="row"><div class="offset-1 span-10">';
		if(isset($_POST['titre'])){
			$listeDocs=Document::findByDonnees($_POST['titre'],$_POST['artiste'],$_POST['categorie'],$_POST['genre']);
			/*$res.='$_POST[titre] :'.$_POST['titre'].', $_POST[artiste] : '.$_POST['artiste'].', $_POST[categorie] :'.$_POST['categorie'].', $_POST[genre] :'.$_POST['genre'];*/
			if(is_array($listeDocs)){
				if(isset($listeDocs[0])){
					$res.='<div class="resultat">';
					foreach($listeDocs as $doc){
						$res.='<article><img src="'.$doc->UrlImage.'" alt="image" /><p>"'.$doc->Titre.'" par '.$doc->Artiste.'</p><p>'.$doc->Descriptif.'</p><div class="'.$doc->Disponibilite.'">'.$doc->Disponibilite.'</div></article>';
					}
					$res.='</div>';
				}
				else{$res.='<div class="warning"><span>Aucun résultat</span></div></div></div>';}
			}
			else{$res.='<div class="warning"><span>Aucun résultat</div></span></div></div>';}
		}

		return $res;
	}

	private function afficheEmprunter(){
		$res='<div class="row" ><div class="offset-1 span-10" >';
		$res.='<h2 class="text-center">Emprunter</h2>
		<form method="post" action="admin.php?action=Emprunter">
			 <fieldset>
			 	<legend>Numéro des documents</legend>
			 	<input type="text" name="numDocs" id="numDocs"';
			 	if(isset($_POST['numDocs'])){$res.=' value="'.$_POST['numDocs'];}
			 	$res.='"/>
			 </fieldset>
			 <fieldset>
			 	<legend>Numéro de l\'adhérent</legend>
			 	<input type="text" name="numAdherent" id="numAdherent"';
			 	if(isset($_POST['numDocs'])){$res.=' value="'.$_POST['numAdherent'];}
			 	$res.='"/>
			 </fieldset>
			
			 <input type="submit" class="btn btn-blue btn-lg " value="Afficher" /></form>';
		
		if (isset($_POST['numDocs2'])){
			$docs=explode(',', $_POST['numDocs2']);
			$adherent=Adherent::findById($_POST['numAdherent2']);
			foreach($docs as $val){
				$document=Document::findByRef($val);
				if(is_object($document) and is_object($adherent)){
					if($document->Disponibilite=="Disponible"){
						$emprunt=new Emprunt();
						$emprunt->IdAdherent=$_POST['numAdherent2'];
						$emprunt->RefDocument=$val;
						$emprunt->insert();
						$document->Disponibilite="Indisponible";
						$document->updateDispo();
						$document=Document::findByRef($val);
			   			$res.='<div class="success">Emprunt confirmé du document '.$document->Titre.' par l\'adhérent 
			   			'.$adherent->Prenom.' '.$adherent->Nom.', le document est maintenant :
			   			 '.$document->Disponibilite.'</span></div></div></div>';
			   		}
			   	}
		   		else if(!is_object($document)){$res.='<div class="error" ><span>Erreur : le document de référence '.$val.' n\'existe pas</span></div></div></div>';}
		   	}
		   	if(!is_object($adherent)){$res.='<p>Erreur : l\'adhérent '.$_POST['numAdherent2'].' n\'est pas inscrit</p></div></div>';}
		   	else{$res.='<div class="error"><span >Le document de référence '.$val.' n\'existe pas</span></div></div></div>';}
		}
		else if (isset($_POST['numDocs'])){
			$res.='<form method="post" action="admin.php?action=Emprunter">
			<input type="hidden" name="numDocs2" id="numDocs2" value="'.$_POST['numDocs'].'"/>
			<input type="hidden" name="numAdherent2" id="numAdherent2" value="'.$_POST['numAdherent'].'"/>';
			$docs=explode(',', $_POST['numDocs']);
			$res.='<div class="resultat" >';
			foreach($docs as $val){
				$doc=Document::findByRef($val);
				if(is_object($doc)){
					$res.='<article><img src="'.$doc->UrlImage.'" alt="image" />
					<p>"'.$doc->Titre.'" par '.$doc->Artiste.'</p>
					<p>'.$doc->Descriptif.'</p>
					<div class="'.$doc->Disponibilite.'" ><p>'.$doc->Disponibilite.'</p>
					</article>';	
				}
				else{$res.='<div class="warning"><span >Le document de référence '.$val.' n\'existe pas</span></div>';}
			}
			$res.='</div></div>';
			$res.='<div class="offset-1 span-10">
					
			<input type="submit" class="btn btn-dark btn-lg" value="Emprunter"></form>
				
			</div></div></div>';
		}
		
		return $res;
	}

	private function AfficheRendu()
	{		
			$res='
				<div class="row">
					<h2 class="text-center">Gèrer les rendus</h2>
					<div class="offset-1 span-10">
					<form method="post" action="admin.php?action=Rendu">
				 <fieldset>
				 	<legend>Numéro des documents</legend>
				 	<input type="text" name="numItems" id="numItems"/>';
				 	$res.='<input type="submit" class="btn btn-green right" value="Rendre" />
				 	</form>
				 	</fieldset>';
				 
				if (isset($_POST['numItems'])){
				$docs=explode(',', $_POST['numItems']);
				foreach($docs as $val){
				$document=Document::findByRef($val);
				$emprunt=Emprunt::findByRef($val);
				if(is_object($document)){
					if ($document->Disponibilite=='Indisponible') {
						$adherent=Adherent::findById($emprunt->IdAdherent);
						$document->Disponibilite="Disponible";
						$document->updateDispo();
						$res.='<div class="info" ><p>'.$adherent->Prenom.' '.$adherent->Nom.' a bien rendu le document '.$document->Titre.'</p></div></div></div>';
						$e=Emprunt::deleteDocument($val);
					}
					else{$res.='<div class="info"><span>Erreur : le document de référence '.$val.' n\'a pas été emprunté</span</div></div></div>';}
				}
				else{$res.='<div class="info"> <span>Le document de référence '.$val.' n\'existe pas</span></div></div></div>';}
			}

		}
		return $res;
	}


	private function nav(){
		$var='<div class="divmenu"><nav>
				<ul class="menu">
					<li><a href="admin.php?action=Emprunter">Gèrer les emprunts</a></li>
					<li><a href="admin.php?action=Rendu">Gèrer les rendus</a></li>
					<li><a href="admin.php?action=Rechercher">Rechercher</a></li>
				</ul>
			<nav></div>';
			return $var;
	}
		private function AfficheHeader(){
    	$var="<img src=\"images/banniere.jpg\" />";
    	return $var;
		}


 		private function AfficheFooter(){
	 	$var="<footer>
        <p>Â&copy; 2014  | Mediatheque Nancy</p>
    	</footer>";
    	return $var;
	 	}


	public function afficheGeneral($selecteur){
		
			$html = '<!DOCTYPE html><html lang="fr"><head>
			<meta charset="UTF-8"><title>Médiathèque de Nancy</title>
			<link rel="stylesheet" type="text/css" href="./stylesheets/style.css">
			</head><body>
			<div class="container" >';
			$html .= "<header>".$this->AfficheHeader()."</header>";
			$html.=$this->nav().'<div class="clear" ></div>';
			switch($selecteur){
			case 'Rechercher':
				$html .= "<div class='recherche'>".$this->afficheRecherche()."</div>";
			break;
			case 'Emprunter':
				$html .= "<div class='recherche'>".$this->afficheEmprunter()."</div>";
			break;

			case 'Rendu':
				$html .= $this->AfficheRendu();
			break;
		}
			$html .= $this->AfficheFooter();
			$html .= "</div></body></html>";

			echo $html;
		}
}
?>