<?php
class Base
{


	private static $db_link ;
	/* 
	établit une connexion a la base et retourne une instance de PDO
	*/
	private static function connect() {
  		$parse = parse_ini_file ( "config.ini" , true ) ;
  		$user = $parse [ "user" ] ;
        $password = $parse [ "password" ] ;
        $hostname = $parse [ "hostname" ] ;
        $bddname = $parse ["dbname"];
        $dsn='mysql:dbname='.$bddname.';host='.$hostname;
		Try {
			$db_link = new PDO($dsn, $user, $password);
		} catch(PDOException $e) {
			throw new PDOException("connection error: ".$e->getMessage(). '<br/>');
		}
		$db_link->exec("SET CHARACTER SET utf8");
		return $db_link;
	}
	/*
	vérifie si un lien existe le crée le cas échéant et le retourne
	*/
	public static function getConnexion() {
		if (isset(self::$db_link)) {
			return self::$db_link ;
		} else {
			self::$db_link = self::connect();
			return self::$db_link ;
		}
	}
}
?>