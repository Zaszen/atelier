<?php

class Categorie {

  private $IdCategorie, $NomC; 

  
  public function __construct() {
    
  }

  public function __toString() {
        return "[". __CLASS__ . "] IdCategorie : ". $this->IdCategorie . ":
				   NomC  ". $this->NomC  ;
  }

  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    throw new Exception($emess, 45);
  }
   
    public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) {
      $this->$attr_name=$attr_val; 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    throw new Exception($emess, 45);
    
  }

  public function save() {
    if (!isset($this->IdCategorie)) {
      return $this->insert();
    } else {
      return $this->update();
    }
  }


  public function update() {
    
    if (!isset($this->IdCategorie)) {
      throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
    } 
    
    $save_query = "update Categories set NomC=".(isset($this->NomC) ? "'$this->NomC'" : "null"). "where IdCategorie=".$this->IdCategorie;
    $pdo = Base::getConnexion();
    $nb=$pdo->exec($save_query);
    
	return $nb;
    
  }

  public function delete() {

    try{
      $query="delete FROM Categories WHERE IdCategorie=".$this->IdCategorie;
      $pdo = Base::getConnexion();
      $dbres = $pdo->exec($query);
    }catch (PDOException $e) {
      throw new PDOException($e->getMessage());
    }

	  return $dbres;
	 
	 
  }
										
  public function insert() {
	  try{
      $query="INSERT INTO Categories VALUES (null,'".$this->NomC."')";
      $pdo = Base::getConnexion();
      $aff_rows = $pdo->exec($query);
      $this->IdCategorie=$pdo->LastInsertId();

      return $aff_rows;
    }catch (PDOException $e) {
      throw new PDOException($e->getMessage());
    }
	
  }
		
    public static function findById($IdCategorie) {
      $query = "select * from Categories where IdCategorie=:IdCategorie";
      //echo $query;

      try{
      $pdo = Base::getConnexion();
      $stmt = $pdo->prepare($query);
      $stmt->execute(array(':IdCategorie' => $IdCategorie));
      
      $dbres=$stmt->fetch(PDO::FETCH_OBJ) ;
     
        $a = new Categorie();
        $a->IdCategorie= $dbres->IdCategorie;
        $a->NomC= $dbres->NomC;

      }catch (PDOException $e) {
        throw new PDOException($e->getMessage());
      }
	   return $a;
	   
    }

     public static function findAll() {

       try {
        $query = "select * from Categories " ;
        $pdo = Base::getConnexion();
        $res=$pdo->query($query);
        $tres=$res->fetchall(PDO::FETCH_OBJ);
        $tab=array();
        foreach($tres as $c){
          $o=new Categorie();
          $o->IdCategorie=$c->IdCategorie;
          $o->NomC=$c->NomC;
          $tab[]=$o;
        }
      } catch (PDOException $e) {
       echo $query . "<br>";
      throw new PDOException("Erreur requête".$e>getMessage());
      }
      return $tab;
    }
}



?>
