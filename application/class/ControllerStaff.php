<?php
class ControllerStaff{

	private function actionRecherche(){
		$vue=new VueStaff();

		$listDocs=Document::findAll();
		$listCats=Categorie::findAll();
		$listGenres=Genre::findAll();

		$vue->listCats=$listCats;
		$vue->listGenres=$listGenres;
		
		echo $vue->afficheGeneral('Rechercher');
	}

	private function actionEmprunter(){
		$vue=new VueStaff();

		$listDocs=Document::findAll();
		$listCats=Categorie::findAll();
		$listGenres=Genre::findAll();

		$vue->listCats=$listCats;
		$vue->listGenres=$listGenres;
		
		echo $vue->afficheGeneral('Emprunter');
	}

	private function actionRendu(){
		$v=new VueStaff();

		$listDocs=Document::findAll();
		$listCats=Categorie::findAll();
		$listGenres=Genre::findAll();

		$v->listCats=$listCats;
		$v->listGenres=$listGenres;

		echo $v->afficheGeneral('Rendu');
	}
/* exemple

	private function actionDetail($get){
		$vue=new Vue();

		$c=Categorie::findById($get['cat_id']);
		$listb=Billets::findAll();
		$b=Billets::findById($get['id']);
		$listbc=Billets::findParCategorie($get['cat_id']);
		$listc=Categorie::findAll();

		$vue->listBillets=$listb;
		$vue->categorie=$c;
		$vue->billet=$b;
		$vue->listcat=$listc;
		
		echo $vue->afficheGeneral('DetailBillet');
	}

*/

	public function dispatch($get){
		if(isset($get['action'])){
			switch($get['action']){
				case 'Rechercher':
					$this->actionRecherche();
				break;
				case 'Emprunter':
					$this->actionEmprunter();
				break;

				case 'Rendu':
					$this->actionRendu();
				break;
				/*exemple
				case 'DetailBillet':
					$this->actionDetail($get);
				break;*/
			}
		}
		else {$this->actionRecherche();}
	 }
}
?>
