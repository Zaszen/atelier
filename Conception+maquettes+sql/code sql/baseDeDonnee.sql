-- phpMyAdmin SQL Dump
-- version 4.1.9
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Sam 08 Novembre 2014 à 12:54
-- Version du serveur :  5.1.73
-- Version de PHP :  5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `elharti1u`
--

-- --------------------------------------------------------

--
-- Structure de la table `Adherents`
--

CREATE TABLE IF NOT EXISTS `Adherents` (
  `IdAdherent` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) NOT NULL,
  `Prenom` varchar(50) NOT NULL,
  `Adress` varchar(150) NOT NULL,
  `email` varchar(50) NOT NULL,
  `Tel` varchar(15) NOT NULL,
  PRIMARY KEY (`IdAdherent`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `Adherents`
--

INSERT INTO `Adherents` (`IdAdherent`, `Nom`, `Prenom`, `Adress`, `email`, `Tel`) VALUES
(1, 'El harti', 'Ayoub', 'Medreville', 'n36@hotmail.fr', '0664885206'),
(2, 'Hilcenko', 'Franck', 'Nancy', 'frank@hotmail.fr', ''),
(3, 'Thal', 'william', 'Medreville', 'william.thal@hotmail.fr', ''),
(4, 'Amanite', 'laurine', 'Nancy', 'amanite.laurine@hotmail.fr', '');

-- --------------------------------------------------------

--
-- Structure de la table `Categories`
--

CREATE TABLE IF NOT EXISTS `Categories` (
  `IdCategorie` int(11) NOT NULL AUTO_INCREMENT,
  `NomC` varchar(20) NOT NULL,
  PRIMARY KEY (`IdCategorie`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `Categories`
--

INSERT INTO `Categories` (`IdCategorie`, `NomC`) VALUES
(1, 'Livre'),
(2, 'DVD'),
(3, 'CD');

-- --------------------------------------------------------

--
-- Structure de la table `Documents`
--

CREATE TABLE IF NOT EXISTS `Documents` (
  `RefDocument` int(11) NOT NULL AUTO_INCREMENT,
  `Titre` varchar(50) NOT NULL,
  `Artiste` varchar(30) NOT NULL,
  `Descriptif` varchar(5000) NOT NULL,
  `IdGenre` int(11) NOT NULL,
  `IdCategorie` int(11) NOT NULL,
  `UrlImage` varchar(200) NOT NULL,
  `Disponibilite` varchar(30) NOT NULL,
  PRIMARY KEY (`RefDocument`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=70 ;

--
-- Contenu de la table `Documents`
--

INSERT INTO `Documents` (`RefDocument`, `Titre`, `Artiste`, `Descriptif`, `IdGenre`, `IdCategorie`, `UrlImage`, `Disponibilite`) VALUES
(1, '1984', 'George Orwell', 'L’histoire se passe à Londres en 1984, comme l''indique le titre du roman. Le monde, depuis les grandes guerres nucléaires des années 1950, est divisé en trois grands « blocs » : l’Océania (Amériques, îles de l''Atlantique (comprenant les îles Anglo-Celtes), Océanie et Afrique australe), l’Eurasia (Europe et URSS) et l’Estasia (Chine et ses contrées méridionales, îles du Japon, et une portion importante mais variable de la Mongolie, la Mandchourie et du Tibet5) qui sont en guerre perpétuelle les uns contre les autres. Ces trois grandes puissances sont dirigées par différents régimes totalitaires revendiqués comme tels, et s''appuyant sur des idéologies nommées différemment mais fondamentalement similaires : l’Angsoc (ou « socialisme anglais ») pour l''Océania, le « néo-bolchévisme » pour l''Eurasia et le « culte de la mort » (ou « oblitération du moi ») pour l''Estasia. Tous ces partis sont présentés comme communistes avant leur montée au pouvoir, jusqu''à ce qu''ils deviennent des régimes totalitaires et relèguent les prolétaires qu''ils prétendaient défendre au bas de la pyramide sociale.', 9, 1, 'images/Documents/1984.jpg', 'Disponible'),
(3, 'Wolverine', 'James Mangold', 'Wolverine (Hugh Jackman), le personnage le plus emblématique de l''univers des X-Men, est entraîné dans une aventure ultime au coeur du Japon contemporain. Plongé dans un monde qu''il ne connaît pas, il doit faire face au seul ennemi de son envergure, dans une bataille à la vie à la mort. Vulnérable pour la première fois et poussé au bout de ses limites physiques et émotionnelles, Wolverine affrontera non seulement l''acier mortel du samouraï mais aussi les questions liées à sa propre immortalité.', 2, 2, 'images/Documents/Wolverine.jpg', 'Disponible'),
(4, '2001, l''Odyssée de l''espace', 'Stanley Kubrick', 'A l''aube de l''Humanité, dans le désert africain, une tribu de primates subit les assauts répétés d''une bande rivale, qui lui dispute un point d''eau. La découverte d''un monolithe noir inspire au chef des singes assiégés un geste inédit et décisif. Brandissant un os, il passe à l''attaque et massacre ses adversaires. Le premier instrument est né.\r\nEn 2001, quatre millions d''années plus tard, un vaisseau spatial évolue en orbite lunaire au rythme langoureux du "Beau Danube Bleu". A son bord, le Dr. Heywood Floyd enquête secrètement sur la découverte d''un monolithe noir qui émet d''étranges signaux vers Jupiter.\r\nDix-huit mois plus tard, les astronautes David Bowman et Frank Poole font route vers Jupiter à bord du Discovery. Les deux hommes vaquent sereinement à leurs tâches quotidiennes sous le contrôle de HAL 9000, un ordinateur exceptionnel doué d''intelligence et de parole. Cependant, HAL, sans doute plus humain que ses maîtres, commence à donner des signes d''inquiétude : à quoi rime cette mission et que risque-t-on de découvrir sur Jupiter ?', 9, 2, 'images/Documents/2001.jpg', 'Disponible'),
(2, 'Le Rouge et le Noir', 'Stendhal', 'Le roman est divisé en deux parties : la première partie retrace le parcours de Julien Sorel en province à Verrières puis à Besançon et plus précisément son entrée chez les de Rênal, de même que son séjour dans un séminaire ; la seconde partie porte sur la vie du héros à Paris comme secrétaire du marquis de La Mole.', 3, 1, 'images/Documents/le_rouge_et_le_noir.jpg', 'Disponible'),
(5, 'Le Bonheur de la tentation', 'Hubert-Félix Thiéfaine', 'Le Bonheur de la tentation est le douzième album d''Hubert-Félix Thiéfaine et fut conçu conjointement avec le précédent, La Tentation du bonheur.', 11, 3, 'images/Documents/Thiefaine.jpg', 'Disponible'),
(6, 'La Tour des Illusions', 'Anthelme Hauchecorne', 'Fuyant un mari machiste et violent, Myriam trouve refuge auprès d une communauté de sans-abri. Avec sa fille Charlotte elle partage leur squat, entre beuveries, bastons et camaraderie. Elle fait la rencontre de Justin, indécrottable poivrot qui la prend sous son aile. Peut-elle compter sur lui ? Mais a-t-elle seulement le choix ? Ensemble, ils essaient de survivre dans cette Cour des Miracles régie par Hugues, patriarche trop gentil pour être honnête. Mais la rue n est pas un dîner de gala, aussi doivent-ils se méfier du Diablotin et de sa bande de voyous défoncés. Quels secrets cache Justin ? Comment a-t-il atterri ici ? Quand cinquante SDF disparaissent sans laisser de traces, le mystère s épaissit. Myriam devra trouver seule les réponses, en passant par les ténèbres et la folie... De troublantes expériences menées au coeur de l inquiétante tour d un milliardaire, un laboratoire souterrain, des égouts où même des mercenaires endurcis regimbent à s aventurer, des créatures meurtrières. Horreur, trahison, cynisme... ...Jusqu où ira-t-elle pour retrouver sa fille ?', 7, 1, 'images/Documents/LaTour.jpg', 'Disponible'),
(7, 'Âmes de Verre', 'Anthelme Hauchecorne', 'Ce livre vous attendait. Il était écrit que vous feriez sa connaissance. Car peut-être êtes-vous, à votre insu, un(e) Éveillé(e).\r\n\r\nAuquel cas, vous êtes en grand danger. Les rues de cette ville ne sont pas sûres. Pour vous, moins que pour tout autre.\r\n\r\nCar les Streums rôdent, à l’affût d’une âme à briser. Je ne vous mentirai pas : vos options ne sont pas légion. Votre meilleure chance de survie git selon toute probabilité entre ces pages.\r\n\r\nQui sont les Streums, demanderez-vous ? Pourquoi convoitent-ils les fragments du Requiem du Dehors ? Quel avantage espèrent-ils retirer de cette partition funeste ?\r\n\r\nSi vous ignorez les réponses à ces questions, vous vous trouvez alors face à un choix. Pour lequel il est de mon devoir de vous aiguiller.\r\n\r\nSouhaitez-vous rejoindre la Vigie, risquer votre vie et sans doute plus encore, dans une lutte désespérée pour déjouer les intrigues du Sidh ? …Ou bien demeurer parmi le troupeau des Dormeurs, à jamais ?\r\nPareille aventure ne se présente qu’une fois. Sachez la saisir.\r\n\r\nEnki, enquêteur et logicien de la Vigie', 2, 1, 'images/Documents/AmesDeVerre.jpg', 'Disponible'),
(8, 'L''Affaire Charles Dexter Ward', 'H. P. Lovecraft', 'L''Affaire Charles Dexter Ward (titre original : The Case of Charles Dexter Ward) est une longue nouvelle fantastique de Howard Phillips Lovecraft, s''inscrivant dans l''univers du mythe de Cthulhu. Il s''agit d''un des plus longs textes de Lovecraft - plus de 50 000 mots -, situé entre le roman et la nouvelle. Écrit au début 1927, il sera publié en décembre 1941 dans Weird Tales (Vol. 35, No. 9). Non édité du vivant de Lovecraft, c''est sans doute l''un des textes majeurs de l''auteur.', 2, 1, 'images/Documents/affaire-charles-dexter-ward.jpg', 'Disponible'),
(9, 'L''Appel de Cthulhu', 'H. P. Lovecraft', 'L''intrigue non linéaire nous est présentée au travers d''un ensemble de documents retrouvé dans les papiers du défunt Francis Wayland Thurston, un anthropologiste qui a enquêté sur un culte obscur.\r\nL''horreur d''argile (The Horror in Clay)\r\nLe récit de l''inspecteur Legrasse (The Tale of Inspector Legrasse)\r\nLa démence qui vint de la mer (The Madness from the Sea)', 2, 1, 'images/Documents/cthulhu.jpg', 'Disponible'),
(10, 'Menteur Menteur', 'Tom Shadyac', 'Fletcher Reede, un père de famille, ne peut s''empêcher de mentir encore et toujours. C''est d''ailleurs pour cela qu''il exerce le métier d''avocat dans l''État de Californie dans lequel il exalte et ne connaît nulle défaite. Alors que l''affaire qui lui permettrait d''accéder à la direction de son cabinet se présente à lui, quelques soucis viennent contrecarrer ses projets. Le jour de son anniversaire, son fils Max souhaite que pendant une journée entière, son père ne puisse rien dire d''autre que la vérité... Dès lors, pendant une journée entière, ce dernier découvrira la difficulté que représente le fait de toujours dire la vérité et ce que le mensonge peut détruire.', 8, 2, 'images/Documents/Menteur-menteur.jpg', 'Disponible');

-- --------------------------------------------------------

--
-- Structure de la table `Emprunts`
--

CREATE TABLE IF NOT EXISTS `Emprunts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdAdherent` int(11) NOT NULL,
  `RefDocument` int(11) NOT NULL,
  `DateEmprunt` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

--
-- Structure de la table `Genres`
--

CREATE TABLE IF NOT EXISTS `Genres` (
  `IdGenre` int(11) NOT NULL AUTO_INCREMENT,
  `NomG` varchar(20) NOT NULL,
  PRIMARY KEY (`IdGenre`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `Genres`
--

INSERT INTO `Genres` (`IdGenre`, `NomG`) VALUES
(1, 'Aventure'),
(2, 'Fantastique'),
(3, 'Amour'),
(4, 'Policier'),
(5, 'Géographie'),
(6, 'Guerre'),
(7, 'Psychologie'),
(8, 'Humour'),
(9, 'Science-Fiction'),
(10, 'Metal'),
(11, 'Chansons françaises'),
(12, 'Pop'),
(13, 'Classique'),
(14, 'Rap/RnB');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
